<?php

namespace Wuduo\ComposerTest;

class ComposerTest
{
    public $start = 'hello world!'. PHP_EOL;

    public function __construct()
    {
        echo __CLASS__ . '初始化' . PHP_EOL;
    }

    public function tests()
    {
        echo __CLASS__ . '->' .__FUNCTION__ . PHP_EOL;
    }

    public static function testsStatic()
    {
        echo __CLASS__ . '::' .__FUNCTION__ . PHP_EOL;
    }

    public static function ok()
    {
        echo __CLASS__ . '::' .__FUNCTION__ . PHP_EOL;
    }
}